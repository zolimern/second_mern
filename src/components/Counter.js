import React, { useReducer, useState } from 'react'
import { reducer } from '../reducer'

export const Counter = () => {
    const initialState = { first: 0, second: 0}



    const [count, dispatch ] = useReducer(reducer, initialState)
  return (



    <div>
        {count.first}

        <button onClick={() => dispatch({ type: 'increase', value: 1})}>Increase</button>
        <button onClick={() => dispatch({type: 'decrement', value: 1})}>Decrease</button>
        <button onClick={() => dispatch({type :'reset', value: 0})}>Reset</button>
    </div>
  )
}
