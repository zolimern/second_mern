

export const reducer = (state, action) => {

    switch(action.type){

        case 'increase':
            return { ...state, first : state.first + action.value}
        case 'decrement':
            return {...state,  first: state.first - action.value  }
        case 'reset':
            return {...state, first: action.value, second: action.value}
        default:
            return state

    }
}